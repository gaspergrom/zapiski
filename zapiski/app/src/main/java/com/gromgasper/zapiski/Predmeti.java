package com.gromgasper.zapiski;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Predmeti extends AppCompatActivity {
    Map<String, ArrayList<Predmet>> predmeti = new HashMap<>();
    static String name, letnik;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_predmeti);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent in = getIntent();
        if (in != null) {
            Bundle b = in.getExtras();
            if (b != null) {
                name = b.getString("letnik");
                letnik = b.getString("letnik2");
            }
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        fillCourses();
        showCourses(predmeti.get(name));
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void fillCourses() {
        ArrayList<Predmet> c1 = new ArrayList<Predmet>();
        c1.add(new Predmet(0, "Programiranje1"));
        c1.add(new Predmet(1, "Osnove matematične analize"));
        c1.add(new Predmet(2, "Diskretne strukture"));
        c1.add(new Predmet(3, "Osnove digitalnih vezij"));
        c1.add(new Predmet(4, "Fizika"));
        c1.add(new Predmet(5, "Programiranje2"));
        c1.add(new Predmet(6, "Linearna algebra"));
        c1.add(new Predmet(7, "arhitektura računalniških sistemov"));
        c1.add(new Predmet(8, "računalniške komunikacije"));
        c1.add(new Predmet(9, "osnove informacijskih sistemov"));
        predmeti.put("letnik1", c1);

        ArrayList<Predmet> c2 = new ArrayList<Predmet>();
        c2.add(new Predmet(10, "matematično modeliranje"));
        c2.add(new Predmet(11, "računalniške tehnologije"));
        c2.add(new Predmet(12, "principi programskih jezikov"));
        c2.add(new Predmet(13, "algoritmi in podatkovne strukture 1"));
        c2.add(new Predmet(14, "osnove podatkovnih baz"));
        c2.add(new Predmet(15, "verjetnost in statistika"));
        c2.add(new Predmet(16, "izračunljivost in računska zahtevnost"));
        c2.add(new Predmet(17, "algoritmi in podatkovne strukture 2"));
        c2.add(new Predmet(18, "teorija informacijskih sistemov"));
        c2.add(new Predmet(19, "operacijski sistemi"));
        c2.add(new Predmet(20, "organizacija računalniških sistemov"));
        predmeti.put("letnik2", c2);

        ArrayList<Predmet> c3 = new ArrayList<Predmet>();
        c3.add(new Predmet(21, "elektronsko poslovanje"));
        c3.add(new Predmet(22, "organizacija in management"));
        c3.add(new Predmet(23, "poslovna inteligenca"));
        c3.add(new Predmet(24, "tehnologija upravljanja podatkov"));
        c3.add(new Predmet(25, "razvoj informacijskih sistemov"));
        c3.add(new Predmet(26, "planiranje in upravljanje informatike"));
        c3.add(new Predmet(27, "postopki razvoja programske opreme"));
        c3.add(new Predmet(28, "spletno programiranje"));
        c3.add(new Predmet(29, "tehnologija programske opreme"));
        c3.add(new Predmet(30, "komunikacijski protokoli"));
        c3.add(new Predmet(31, "brezžična in mobilna omrežja"));
        c3.add(new Predmet(32, "modeliranje računalniških omrežij"));
        c3.add(new Predmet(33, "zanesljivost in zmogljivost računalniških omrežij"));
        c3.add(new Predmet(34, "digitalno načrtovanje"));
        c3.add(new Predmet(35, "porazdeljeni sistemi"));
        c3.add(new Predmet(36, "prevajalniki"));
        c3.add(new Predmet(37, "sistemska programska oprema"));
        c3.add(new Predmet(38, "računska zahtevnost in hevristično programiranje"));
        c3.add(new Predmet(39, "razvoj inteligentnih sistemov"));
        c3.add(new Predmet(40, "inteligentni sistemi"));
        c3.add(new Predmet(41, "umetno zaznavanje"));
        c3.add(new Predmet(42, "osnove oblikovanja"));
        c3.add(new Predmet(43, "multimedijski sistemi"));
        c3.add(new Predmet(44, "računalniška grafika in tehnologija iger"));
        c3.add(new Predmet(45, "osnove umetne inteligence"));
        c3.add(new Predmet(46, "ekonomika in podjetništvo"));
        predmeti.put("letnik3", c3);

        ArrayList<Predmet> c4 = new ArrayList<Predmet>();
        c4.add(new Predmet(47, "Osnove vrjetnosti in statistike"));
        c4.add(new Predmet(48, "Operacijski sistemi"));
        c4.add(new Predmet(49, "Računalniške komunikacije"));
        c4.add(new Predmet(51, "Programiranje 1"));
        c4.add(new Predmet(52, "Podatkovne baze"));
        c4.add(new Predmet(53, "Programiranje 2"));
        c4.add(new Predmet(54, "Diskretne strukture"));
        c4.add(new Predmet(55, "Matematika"));
        c4.add(new Predmet(56, "Uvod v računalništvo"));
        c4.add(new Predmet(57, "Računalniška arhitektura"));
        predmeti.put("letnik4", c4);

        ArrayList<Predmet> c5 = new ArrayList<Predmet>();
        c5.add(new Predmet(58, "algoritmi in podatkovne strukture 1"));
        c5.add(new Predmet(59, "algoritmi in podatkovne strukture 2"));
        c5.add(new Predmet(50, "projektni praktikum"));
        c5.add(new Predmet(61, "elektronsko in mobilno poslovanje"));
        c5.add(new Predmet(62, "podatkovne baze 2"));
        c5.add(new Predmet(63, "informacijski sistemi"));
        c5.add(new Predmet(64, "grafično oblikovanje"));
        c5.add(new Predmet(65, "komunikacijski protokoli in omrežna varnost"));
        c5.add(new Predmet(66, "organizacija računalnikov"));
        c5.add(new Predmet(67, "digitalna vezja"));
        c5.add(new Predmet(68, "računalniška grafika"));
        c5.add(new Predmet(69, "umetna inteligenca"));
        c5.add(new Predmet(70, "uporabniški vmesniki"));
        c5.add(new Predmet(71, "prevajalniki in navidezni stroji"));
        c5.add(new Predmet(72, "testiranje in kakovost"));
        c5.add(new Predmet(73, "razvoj informacijskih sistemov"));
        c5.add(new Predmet(74, "produkcija multimedijskih gradiv"));
        c5.add(new Predmet(75, "digitalno procesiranje signalov"));
        c5.add(new Predmet(76, "spletne tehnologije"));
        c5.add(new Predmet(77, "vhodno-izhodne naprave"));
        c5.add(new Predmet(78, "načrtovanje digitalnih naprav"));
        c5.add(new Predmet(79, "podatkovno rudarjenje"));
        c5.add(new Predmet(80, "izbrana poglavja iz računalništva in informatike"));
        c5.add(new Predmet(81, "tehnologija programske opreme"));
        c5.add(new Predmet(82, "planiranje in upravljanje informatike"));
        c5.add(new Predmet(83, "multimedijske tehnologije"));
        c5.add(new Predmet(84, "vzporedni in porazdljeni sistemi in algoritmi"));
        c5.add(new Predmet(85, "sistemska programska oprema"));
        c5.add(new Predmet(86, "procesna avtomatika"));
        c5.add(new Predmet(87, "vgrajeni sistemi"));
        c5.add(new Predmet(88, "robotika in računalniško zaznavanje"));
        c5.add(new Predmet(89, "tehnologija iger in navidezna resničnost"));
        c5.add(new Predmet(90, "odločitveni sistemi"));
        c5.add(new Predmet(91, "numerične metode"));
        predmeti.put("letnik5", c5);

    }

    protected void showCourses(ArrayList l) {
        LinearLayout yourlayout = (LinearLayout) findViewById(R.id.courses_layout);

        for (int i = 0; i < l.size(); i++) {
            Button btn = new Button(Predmeti.this);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(10, 15, 10, 15);
            btn.setLayoutParams(params);
            //btn.setHeight(50);
            Predmet t = (Predmet) l.get(i);
            btn.setText(t.name);
            btn.setBackgroundResource(R.drawable.stroke);
            yourlayout.addView(btn);
            btn.setTag(t.id);
            btn.setOnClickListener(predmetiClick);
        }
    }

    View.OnClickListener predmetiClick = new View.OnClickListener() {
        public void onClick(View v) {
            Intent i = new Intent(Predmeti.this, listing.class);
            Bundle b = new Bundle();
            b.putString("id", v.getTag().toString());
            b.putString("letnik", letnik);
            b.putString("predmet", ((Button) v).getText().toString());
            i.putExtras(b);
            startActivity(i);
        }
    };
}

class Predmet {
    public int id;
    public String name;

    Predmet(int id, String ime) {
        this.id = id;
        this.name = ime;
    }
}