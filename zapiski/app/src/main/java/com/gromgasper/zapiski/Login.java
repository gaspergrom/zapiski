package com.gromgasper.zapiski;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

public class Login extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Button log = (Button) findViewById(R.id.loginbtn);
        final EditText emailinp = (EditText) findViewById(R.id.mail);
        final EditText passinp = (EditText) findViewById(R.id.pass);
        log.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = emailinp.getText().toString();
                String pass = passinp.getText().toString();
                emailinp.setEnabled(false);
                emailinp.setEnabled(true);
                passinp.setEnabled(false);
                passinp.setEnabled(true);
                if (email.length() == 6) {
                    if (pass.length() > 0) {
                        if (Character.isLetter(email.charAt(0)) & Character.isLetter(email.charAt(1))) {
                            if (Character.isDigit(email.charAt(2)) & Character.isDigit(email.charAt(3)) & Character.isDigit(email.charAt(4)) & Character.isDigit(email.charAt(5))) {

                                email += "@student.uni-lj.si";
                                new CheckUser().execute(email, pass);

                            } else {
                                Snackbar.make(view, "Chars from 3-6 have to be digits", Snackbar.LENGTH_LONG)
                                        .setAction("Action", null).show();
                            }
                        } else {
                            Snackbar.make(view, "First 2 chars should be letters", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                        }
                    } else {
                        Snackbar.make(view, "Please enter password", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                } else {
                    Snackbar.make(view, "Please enter email address", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                }

            }
        });
        TextView r = (TextView) findViewById(R.id.reg);
        r.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Login.this, register.class);
                startActivity(i);
            }
        });
    }

    class CheckUser extends AsyncTask {
        private String result;

        public CheckUser() {
            this.result = "";
        }

        @Override
        protected String doInBackground(Object[] objects) {
            try {
                String link = "http://gromgasper.com/zapiski/login.php";

                String data = URLEncoder.encode("email", "UTF-8") + "=" +
                        URLEncoder.encode((String) objects[0], "UTF-8");
                data += "&" + URLEncoder.encode("password", "UTF-8") + "=" +
                        URLEncoder.encode((String) objects[1], "UTF-8");

                URL url = new URL(link);
                URLConnection conn = url.openConnection();

                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

                wr.write(data);
                wr.flush();

                BufferedReader reader = new BufferedReader(new
                        InputStreamReader(conn.getInputStream()));

                StringBuilder sb = new StringBuilder();
                String line = null;

                // Read Server Response
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
//                    break;
                }

                result = sb.toString();

            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(Object o) {
            String result = (String) o;

            if (!result.equals("")){
                String filename = "login";
                FileOutputStream outputStream;

                try {
                    outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
                    outputStream.write(result.getBytes());
                    outputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Intent login = new Intent(Login.this, main.class);
                Login.this.startActivity(login);
                Login.this.finish();
            } else{
                new AlertDialog.Builder(Login.this)
                        .setMessage("napačno uporabniško ime ali geslo")
                        .show();
            }
        }
    }
}
