package com.gromgasper.zapiski;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

public class register extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        TextView t=(TextView)findViewById(R.id.log);
        t.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Button b=(Button) findViewById(R.id.regbtn);
        final EditText emailinp=(EditText)findViewById(R.id.mail);
        final EditText passinp=(EditText)findViewById(R.id.pass);
        final EditText repassinp=(EditText)findViewById(R.id.repass);
        final EditText nameinp=(EditText)findViewById(R.id.name);

        b.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {

                String email=emailinp.getText().toString();
                String pass=passinp.getText().toString();
                String repass=repassinp.getText().toString();
                String name=nameinp.getText().toString();

                emailinp.setEnabled(false);
                emailinp.setEnabled(true);
                passinp.setEnabled(false);
                passinp.setEnabled(true);
                repassinp.setEnabled(false);
                repassinp.setEnabled(true);
                nameinp.setEnabled(false);
                nameinp.setEnabled(true);

                if(email.length()==6){
                    if(pass.length()>0){
                        if(repass.length()>0){
                            if(name.length()>0){
                                if(Character.isLetter(email.charAt(0)) & Character.isLetter(email.charAt(1))){
                                    if(Character.isDigit(email.charAt(2)) & Character.isDigit(email.charAt(3)) & Character.isDigit(email.charAt(4)) & Character.isDigit(email.charAt(5))){
                                        if(pass.equals(repass)) {

                                            email += "@student.uni-lj.si";
                                            new InsertUser().execute(email, name, pass);

//                                            if (insertUser(email, pass, name)) {
//                                                finish();
//                                            }
//                                            else{
//                                                Snackbar.make(view, "something went wrong please try again", Snackbar.LENGTH_LONG)
//                                                        .setAction("Action", null).show();
//                                            }2a334
                                        }

                                        else{
                                            Snackbar.make(view, "passwords don't match", Snackbar.LENGTH_LONG)
                                                    .setAction("Action", null).show();
                                        }
                                    }
                                    else{
                                        Snackbar.make(view, "last 4 chars in email have to be digits", Snackbar.LENGTH_LONG)
                                                .setAction("Action", null).show();
                                    }
                                }else{
                                    Snackbar.make(view, "First 2 chars in email should be letters", Snackbar.LENGTH_LONG)
                                            .setAction("Action", null).show();
                                }
                            }
                            else{
                                Snackbar.make(view, "Please repeat password", Snackbar.LENGTH_LONG)
                                        .setAction("Action", null).show();
                            }
                        }
                        else{
                            Snackbar.make(view, "Please repeat password", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                        }
                    }
                    else{
                        Snackbar.make(view, "Please enter password", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                }else{
                    Snackbar.make(view, "Please enter email address", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                }
            }
        });
    }

    class InsertUser extends AsyncTask {
        private String result;

        public InsertUser() {
            this.result = "";
        }

        @Override
        protected String doInBackground(Object[] objects) {
            try {
                String link = "http://gromgasper.com/zapiski/register.php";

                String data = URLEncoder.encode("email", "UTF-8") + "=" +
                        URLEncoder.encode((String) objects[0], "UTF-8");
                data += "&" + URLEncoder.encode("name", "UTF-8") + "=" +
                        URLEncoder.encode((String) objects[1], "UTF-8");
                data += "&" + URLEncoder.encode("password", "UTF-8") + "=" +
                        URLEncoder.encode((String) objects[2], "UTF-8");

                URL url = new URL(link);
                URLConnection conn = url.openConnection();

                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

                wr.write(data);
                wr.flush();

                BufferedReader reader = new BufferedReader(new
                        InputStreamReader(conn.getInputStream()));

                StringBuilder sb = new StringBuilder();
                String line = null;

                // Read Server Response
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                    break;
                }

                result = sb.toString();

            } catch (Exception e) {
                result = e.toString();
            }
            return result;
        }

        @Override
        protected void onPostExecute(Object o) {
            String result = (String) o;

            if (result.equals("registered")){
                finish();
            } else{
                new AlertDialog.Builder(register.this)
                        .setMessage("ta uporabnik že obstaja")
                        .show();
            }
        }
    }
}
