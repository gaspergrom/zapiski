package com.gromgasper.zapiski;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

public class list extends AppCompatActivity {
    String[] courses = new String[92];
    String id, zapiski, letnik, predmet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent in = getIntent();
        if (in != null) {
            Bundle bundle = in.getExtras();
            if (bundle != null) {
                id = bundle.getString("id");
                zapiski = bundle.getString("zap");
                letnik = bundle.getString("letnik");
                predmet = bundle.getString("predmet");
            }
        }

        getSupportActionBar().setTitle((zapiski).toUpperCase());
        new GetFiles().execute();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    class GetFiles extends AsyncTask {
        private String result;

        public GetFiles() {
            this.result = "";
        }

        @Override
        protected String doInBackground(Object[] objects) {
            String z = "0";
            try {
                if (zapiski.equals("zapiski"))
                    z = "1";

                String link = "http://gromgasper.com/zapiski/get_files.php";

                String data = URLEncoder.encode("id", "UTF-8") + "=" +
                        URLEncoder.encode(id, "UTF-8");
                data += "&" + URLEncoder.encode("zapiski", "UTF-8") + "=" +
                        URLEncoder.encode(z, "UTF-8");

                URL url = new URL(link);
                URLConnection conn = url.openConnection();

                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

                wr.write(data);
                wr.flush();

                BufferedReader reader = new BufferedReader(new
                        InputStreamReader(conn.getInputStream()));

                StringBuilder sb = new StringBuilder();
                String line = null;

                // Read Server Response
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }

                result = sb.toString();

            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(Object o) {
            String[] files = ((String) o).split("\n");
            if (!files[0].equals(""))
                for (String f : files)
                    addBtn(f);
        }
    }

    void addBtn(String path) {
        LinearLayout layout = (LinearLayout) findViewById(R.id.files_layout);

        Button btn = new Button(list.this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(10, 15, 10, 15);
        btn.setLayoutParams(params);
        btn.setText(path.split("/")[1]);
        btn.setTag("http://gromgasper.com/zapiski/" + path);
        btn.setBackgroundResource(R.drawable.stroke);
        btn.setOnClickListener(fileClick);
        layout.addView(btn);
    }

    View.OnClickListener fileClick = new View.OnClickListener() {
        public void onClick(View v) {
            Intent i = new Intent(list.this, zapisek.class);
            Bundle b = new Bundle();
            b.putString("letnik", letnik);
            b.putString("predmet", predmet);
            b.putString("name", ((Button) v).getText().toString());
            b.putString("link", v.getTag().toString());
            i.putExtras(b);
            startActivity(i);
        }
    };

    protected void setCourse() {
        courses[0] = "Programiranje1";
        courses[1] = "Osnove matematične analize";
        courses[2] = "Diskretne strukture";
        courses[3] = "Osnove digitalnih vezij";
        courses[4] = "Fizika";
        courses[5] = "Programiranje2";
        courses[6] = "Linearna algebra";
        courses[7] = "arhitektura računalniških sistemov";
        courses[8] = "računalniške komunikacije";
        courses[9] = "osnove informacijskih sistemov";
        courses[10] = "matematično modeliranje";
        courses[11] = "računalniške tehnologije";
        courses[12] = "principi programskih jezikov";
        courses[13] = "algoritmi in podatkovne strukture 1";
        courses[14] = "osnove podatkovnih baz";
        courses[15] = "verjetnost in statistika";
        courses[16] = "izračunljivost in računska zahtevnost";
        courses[17] = "algoritmi in podatkovne strukture 2";
        courses[18] = "teorija informacijskih sistemov";
        courses[19] = "operacijski sistemi";
        courses[20] = "organizacija računalniških sistemov";
        courses[21] = "elektronsko poslovanje";
        courses[22] = "organizacija in management";
        courses[23] = "poslovna inteligenca";
        courses[24] = "tehnologija upravljanja podatkov";
        courses[25] = "razvoj informacijskih sistemov";
        courses[26] = "planiranje in upravljanje informatike";
        courses[27] = "postopki razvoja programske opreme";
        courses[28] = "spletno programiranje";
        courses[29] = "tehnologija programske opreme";
        courses[30] = "komunikacijski protokoli";
        courses[31] = "brezžična in mobilna omrežja";
        courses[32] = "modeliranje računalniških omrežij";
        courses[33] = "zanesljivost in zmogljivost računalniških omrežij";
        courses[34] = "digitalno načrtovanje";
        courses[35] = "porazdeljeni sistemi";
        courses[36] = "prevajalniki";
        courses[37] = "sistemska programska oprema";
        courses[38] = "računska zahtevnost in hevristično programiranje";
        courses[39] = "razvoj inteligentnih sistemov";
        courses[40] = "inteligentni sistemi";
        courses[41] = "umetno zaznavanje";
        courses[42] = "osnove oblikovanja";
        courses[43] = "multimedijski sistemi";
        courses[44] = "računalniška grafika in tehnologija iger";
        courses[45] = "osnove umetne inteligence";
        courses[46] = "ekonomika in podjetništvo";
        courses[47] = "Osnove vrjetnosti in statistike";
        courses[48] = "Operacijski sistemi";
        courses[49] = "Računalniške komunikacije";
        courses[51] = "Programiranje 1";
        courses[52] = "Podatkovne baze";
        courses[53] = "Programiranje 2";
        courses[54] = "Diskretne strukture";
        courses[55] = "Matematika";
        courses[56] = "Uvod v računalništvo";
        courses[57] = "Računalniška arhitektura";
        courses[58] = "algoritmi in podatkovne strukture 1";
        courses[59] = "algoritmi in podatkovne strukture 2";
        courses[50] = "projektni praktikum";
        courses[61] = "elektronsko in mobilno poslovanje";
        courses[62] = "podatkovne baze 2";
        courses[63] = "informacijski sistemi";
        courses[64] = "grafično oblikovanje";
        courses[65] = "komunikacijski protokoli in omrežna varnost";
        courses[66] = "organizacija računalnikov";
        courses[67] = "digitalna vezja";
        courses[68] = "računalniška grafika";
        courses[69] = "umetna inteligenca";
        courses[70] = "uporabniški vmesniki";
        courses[71] = "prevajalniki in navidezni stroji";
        courses[72] = "testiranje in kakovost";
        courses[73] = "razvoj informacijskih sistemov";
        courses[74] = "produkcija multimedijskih gradiv";
        courses[75] = "digitalno procesiranje signalov";
        courses[76] = "spletne tehnologije";
        courses[77] = "vhodno-izhodne naprave";
        courses[78] = "načrtovanje digitalnih naprav";
        courses[79] = "podatkovno rudarjenje";
        courses[80] = "izbrana poglavja iz računalništva in informatike";
        courses[81] = "tehnologija programske opreme";
        courses[82] = "planiranje in upravljanje informatike";
        courses[83] = "multimedijske tehnologije";
        courses[84] = "vzporedni in porazdljeni sistemi in algoritmi";
        courses[85] = "sistemska programska oprema";
        courses[86] = "procesna avtomatika";
        courses[87] = "vgrajeni sistemi";
        courses[88] = "robotika in računalniško zaznavanje";
        courses[89] = "tehnologija iger in navidezna resničnost";
        courses[90] = "Odločitveni sistemi";
        courses[91] = "Numerične metode";
    }
}
