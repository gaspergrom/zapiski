package com.gromgasper.zapiski;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class zapisek extends AppCompatActivity {
    boolean fav = false;
    String letnik, predmet, name, link;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zapisek);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
                R.layout.action_bar,
                null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(actionBarLayout);

        Intent in = getIntent();
        if (in != null) {
            Bundle bundle = in.getExtras();
            if (bundle != null) {
                letnik = bundle.getString("letnik");
                predmet = bundle.getString("predmet");
                name = bundle.getString("name");
                link = bundle.getString("link");
            }
        }

        Button openZapisek = (Button) findViewById(R.id.buttonZapisek);
        ((TextView)findViewById(R.id.textViewLetnik)).setText(letnik);
        ((TextView)findViewById(R.id.textViewPredmet)).setText(predmet);
        ((TextView)findViewById(R.id.textViewNaslov)).setText(name);

        openZapisek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(link)));
            }
        });

        final Button star = (Button) findViewById(R.id.stars);
        if (fav) {
            star.setBackground(getResources().getDrawable(R.drawable.ic_menu_star));
        } else {
            star.setBackground(getResources().getDrawable(R.drawable.ic_menu_star_full));
        }
        star.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fav = !fav;
                if (fav) {
                    star.setBackground(getResources().getDrawable(R.drawable.ic_menu_star));
                } else {
                    star.setBackground(getResources().getDrawable(R.drawable.ic_menu_star_full));
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
