package com.gromgasper.zapiski;


import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Add extends AppCompatActivity {
    private static final String TAG = "";
    private String[] arraySpinner;
    private String[] pred;
    Map<String, ArrayList<Predmet>> predmeti = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        fillCourses();
        fillLetnik();
        Spinner l = (Spinner) findViewById(R.id.sletnik);
        Spinner p = (Spinner) findViewById(R.id.spredmeti);
        l.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                fillPredmet(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        Button u = (Button) findViewById(R.id.upload);
        u.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                showFileChooser();
            }
        });
    }

    private static final int FILE_SELECT_CODE = 0;

    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    FILE_SELECT_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    Uri uri = data.getData();
//                    Log.d(TAG, "File Uri: " + uri.toString());
                    setText("");
                    new UploadFile().execute();
                    // Get the path
//                    try{
//                        String path = getPath(this, uri);
//                        ((TextView)findViewById(R.id.selected)).setText(path);
//                    } catch (Exception e){
//                        ((TextView)findViewById(R.id.selected)).setText(e.toString());
//                    }
//                    Log.d(TAG, "File Path: " + path);
                    // Get the file instance
                    // File file = new File(path);
                    // Initiate the upload
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    class UploadFile extends AsyncTask {

        @Override
        protected String doInBackground(Object[] objects) {

            try {
                String sourceFileUri = (String) objects[0];

                HttpURLConnection conn = null;
                DataOutputStream dos = null;
                String lineEnd = "\r\n";
                String twoHyphens = "--";
                String boundary = "*****";
                int bytesRead, bytesAvailable, bufferSize;
                byte[] buffer;
                int maxBufferSize = 1 * 1024 * 1024;
                File sourceFile = new File(sourceFileUri);

                if (sourceFile.isFile()) {
                    setText("a");

                    try {
                        String upLoadServerUri = "http://gromgasper.com/zapiski/file_upload.php";

                        // open a URL connection to the Servlet
                        FileInputStream fileInputStream = new FileInputStream(
                                sourceFile);
                        URL url = new URL(upLoadServerUri);

                        // Open a HTTP connection to the URL
                        conn = (HttpURLConnection) url.openConnection();
                        conn.setDoInput(true); // Allow Inputs
                        conn.setDoOutput(true); // Allow Outputs
                        conn.setUseCaches(false); // Don't use a Cached Copy
                        conn.setRequestMethod("POST");
                        conn.setRequestProperty("Connection", "Keep-Alive");
                        conn.setRequestProperty("ENCTYPE",
                                "multipart/form-data");
                        conn.setRequestProperty("Content-Type",
                                "multipart/form-data;boundary=" + boundary);
                        conn.setRequestProperty("bill", sourceFileUri);

                        dos = new DataOutputStream(conn.getOutputStream());

                        dos.writeBytes(twoHyphens + boundary + lineEnd);
                        dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
                                + sourceFileUri + "\"" + lineEnd);

                        dos.writeBytes(lineEnd);

                        // create a buffer of maximum size
                        bytesAvailable = fileInputStream.available();

                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        buffer = new byte[bufferSize];

                        // read file and write it into form...
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                        while (bytesRead > 0) {

                            dos.write(buffer, 0, bufferSize);
                            bytesAvailable = fileInputStream.available();
                            bufferSize = Math
                                    .min(bytesAvailable, maxBufferSize);
                            bytesRead = fileInputStream.read(buffer, 0,
                                    bufferSize);

                        }

                        // send multipart form data necesssary after file
                        // data...
                        dos.writeBytes(lineEnd);
                        dos.writeBytes(twoHyphens + boundary + twoHyphens
                                + lineEnd);

                        // Responses from the server (code and message)
                        int serverResponseCode = conn.getResponseCode();
                        String serverResponseMessage = conn
                                .getResponseMessage();
                        setText(Integer.toString(serverResponseCode) + "\n" + serverResponseMessage);

                        if (serverResponseCode == 200) {

                            // messageText.setText(msg);
                            //Toast.makeText(ctx, "File Upload Complete.",
                            //      Toast.LENGTH_SHORT).show();

                            // recursiveDelete(mDirectory1);

                        }

                        // close the streams //
                        fileInputStream.close();
                        dos.flush();
                        dos.close();

                    } catch (Exception e) {

                        // dialog.dismiss();
                        setText(e.getMessage());

                    }
                    // dialog.dismiss();

                } // End else block


            } catch (Exception ex) {
                // dialog.dismiss();

                setText(ex.getMessage());
            }
            return "Executed";
        }
    }

    void setText(String text){
        ((TextView)findViewById(R.id.selected)).setText(text);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void fillPredmet(int i) {
        String n = "letnik" + Integer.toString(i + 1);
        ArrayList<Predmet> a = predmeti.get(n);
        this.pred = new String[a.size()];
        for (int j = 0; j < a.size(); j++) {
            this.pred[j] = a.get(j).name;
        }

        Spinner s = (Spinner) findViewById(R.id.spredmeti);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, pred);
        s.setAdapter(adapter);
    }

    protected void fillLetnik() {

        this.arraySpinner = new String[]{
                "[UNI] 1. Letnik", "[UNI] 2. Letnik", "[UNI] 3. Letnik", "[VŠ] 1. Letnik", "[VŠ] 2. in 3. Letnik"
        };
        Spinner s = (Spinner) findViewById(R.id.sletnik);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arraySpinner);
        s.setAdapter(adapter);
    }

    protected void fillCourses() {
        ArrayList<Predmet> c1 = new ArrayList<Predmet>();
        c1.add(new Predmet(0, "Programiranje1"));
        c1.add(new Predmet(1, "Osnove matematične analize"));
        c1.add(new Predmet(2, "Diskretne strukture"));
        c1.add(new Predmet(3, "Osnove digitalnih vezij"));
        c1.add(new Predmet(4, "Fizika"));
        c1.add(new Predmet(5, "Programiranje2"));
        c1.add(new Predmet(6, "Linearna algebra"));
        c1.add(new Predmet(7, "arhitektura računalniških sistemov"));
        c1.add(new Predmet(8, "računalniške komunikacije"));
        c1.add(new Predmet(9, "osnove informacijskih sistemov"));
        predmeti.put("letnik1", c1);

        ArrayList<Predmet> c2 = new ArrayList<Predmet>();
        c2.add(new Predmet(10, "matematično modeliranje"));
        c2.add(new Predmet(11, "računalniške tehnologije"));
        c2.add(new Predmet(12, "principi programskih jezikov"));
        c2.add(new Predmet(13, "algoritmi in podatkovne strukture 1"));
        c2.add(new Predmet(14, "osnove podatkovnih baz"));
        c2.add(new Predmet(15, "verjetnost in statistika"));
        c2.add(new Predmet(16, "izračunljivost in računska zahtevnost"));
        c2.add(new Predmet(17, "algoritmi in podatkovne strukture 2"));
        c2.add(new Predmet(18, "teorija informacijskih sistemov"));
        c2.add(new Predmet(19, "operacijski sistemi"));
        c2.add(new Predmet(20, "organizacija računalniških sistemov"));
        predmeti.put("letnik2", c2);

        ArrayList<Predmet> c3 = new ArrayList<Predmet>();
        c3.add(new Predmet(21, "elektronsko poslovanje"));
        c3.add(new Predmet(22, "organizacija in management"));
        c3.add(new Predmet(23, "poslovna inteligenca"));
        c3.add(new Predmet(24, "tehnologija upravljanja podatkov"));
        c3.add(new Predmet(25, "razvoj informacijskih sistemov"));
        c3.add(new Predmet(26, "planiranje in upravljanje informatike"));
        c3.add(new Predmet(27, "postopki razvoja programske opreme"));
        c3.add(new Predmet(28, "spletno programiranje"));
        c3.add(new Predmet(29, "tehnologija programske opreme"));
        c3.add(new Predmet(30, "komunikacijski protokoli"));
        c3.add(new Predmet(31, "brezžična in mobilna omrežja"));
        c3.add(new Predmet(32, "modeliranje računalniških omrežij"));
        c3.add(new Predmet(33, "zanesljivost in zmogljivost računalniških omrežij"));
        c3.add(new Predmet(34, "digitalno načrtovanje"));
        c3.add(new Predmet(35, "porazdeljeni sistemi"));
        c3.add(new Predmet(36, "prevajalniki"));
        c3.add(new Predmet(37, "sistemska programska oprema"));
        c3.add(new Predmet(38, "računska zahtevnost in hevristično programiranje"));
        c3.add(new Predmet(39, "razvoj inteligentnih sistemov"));
        c3.add(new Predmet(40, "inteligentni sistemi"));
        c3.add(new Predmet(41, "umetno zaznavanje"));
        c3.add(new Predmet(42, "osnove oblikovanja"));
        c3.add(new Predmet(43, "multimedijski sistemi"));
        c3.add(new Predmet(44, "računalniška grafika in tehnologija iger"));
        c3.add(new Predmet(45, "osnove umetne inteligence"));
        c3.add(new Predmet(46, "ekonomika in podjetništvo"));
        predmeti.put("letnik3", c3);

        ArrayList<Predmet> c4 = new ArrayList<Predmet>();
        c4.add(new Predmet(47, "Osnove vrjetnosti in statistike"));
        c4.add(new Predmet(48, "Operacijski sistemi"));
        c4.add(new Predmet(49, "Računalniške komunikacije"));
        c4.add(new Predmet(51, "Programiranje 1"));
        c4.add(new Predmet(52, "Podatkovne baze"));
        c4.add(new Predmet(53, "Programiranje 2"));
        c4.add(new Predmet(54, "Diskretne strukture"));
        c4.add(new Predmet(55, "Matematika"));
        c4.add(new Predmet(56, "Uvod v računalništvo"));
        c4.add(new Predmet(57, "Računalniška arhitektura"));
        predmeti.put("letnik4", c4);

        ArrayList<Predmet> c5 = new ArrayList<Predmet>();
        c5.add(new Predmet(58, "algoritmi in podatkovne strukture 1"));
        c5.add(new Predmet(59, "algoritmi in podatkovne strukture 2"));
        c5.add(new Predmet(50, "projektni praktikum"));
        c5.add(new Predmet(61, "elektronsko in mobilno poslovanje"));
        c5.add(new Predmet(62, "podatkovne baze 2"));
        c5.add(new Predmet(63, "informacijski sistemi"));
        c5.add(new Predmet(64, "grafično oblikovanje"));
        c5.add(new Predmet(65, "komunikacijski protokoli in omrežna varnost"));
        c5.add(new Predmet(66, "organizacija računalnikov"));
        c5.add(new Predmet(67, "digitalna vezja"));
        c5.add(new Predmet(68, "računalniška grafika"));
        c5.add(new Predmet(69, "umetna inteligenca"));
        c5.add(new Predmet(70, "uporabniški vmesniki"));
        c5.add(new Predmet(71, "prevajalniki in navidezni stroji"));
        c5.add(new Predmet(72, "testiranje in kakovost"));
        c5.add(new Predmet(73, "razvoj informacijskih sistemov"));
        c5.add(new Predmet(74, "produkcija multimedijskih gradiv"));
        c5.add(new Predmet(75, "digitalno procesiranje signalov"));
        c5.add(new Predmet(76, "spletne tehnologije"));
        c5.add(new Predmet(77, "vhodno-izhodne naprave"));
        c5.add(new Predmet(78, "načrtovanje digitalnih naprav"));
        c5.add(new Predmet(79, "podatkovno rudarjenje"));
        c5.add(new Predmet(80, "izbrana poglavja iz računalništva in informatike"));
        c5.add(new Predmet(81, "tehnologija programske opreme"));
        c5.add(new Predmet(82, "planiranje in upravljanje informatike"));
        c5.add(new Predmet(83, "multimedijske tehnologije"));
        c5.add(new Predmet(84, "vzporedni in porazdljeni sistemi in algoritmi"));
        c5.add(new Predmet(85, "sistemska programska oprema"));
        c5.add(new Predmet(86, "procesna avtomatika"));
        c5.add(new Predmet(87, "vgrajeni sistemi"));
        c5.add(new Predmet(88, "robotika in računalniško zaznavanje"));
        c5.add(new Predmet(89, "tehnologija iger in navidezna resničnost"));
        c5.add(new Predmet(90, "odločitveni sistemi"));
        c5.add(new Predmet(91, "numerične metode"));
        predmeti.put("letnik5", c5);

    }

}
