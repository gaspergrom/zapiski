package com.gromgasper.zapiski;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class main extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(main.this, Add.class);
                startActivity(myIntent);
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        findViewById(R.id.letnik1).setOnClickListener(letnikiClick);
        findViewById(R.id.letnik2).setOnClickListener(letnikiClick);
        findViewById(R.id.letnik3).setOnClickListener(letnikiClick);
        findViewById(R.id.letnik4).setOnClickListener(letnikiClick);
        findViewById(R.id.letnik5).setOnClickListener(letnikiClick);
        Button fav = (Button) findViewById(R.id.fav1);
        fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(main.this, zapisek.class);
                startActivity(i);
            }
        });
        checkLogin(navigationView.getHeaderView(0));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        View inc_main = findViewById(R.id.include_main);
        View inc_letniki = findViewById(R.id.include_letniki);
        if (id == R.id.nav_zapiski) {
            inc_letniki.setVisibility(View.GONE);
            inc_main.setVisibility(View.VISIBLE);

        } else if (id == R.id.nav_favourites) {
            inc_main.setVisibility(View.GONE);
            inc_letniki.setVisibility(View.VISIBLE);

        } else if (id == R.id.nav_signout) {
            getApplicationContext().deleteFile("login");
            Intent in = new Intent(main.this, Login.class);
            startActivity(in);
            main.this.finish();
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    View.OnClickListener letnikiClick = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(main.this, Predmeti.class);
            Bundle b = new Bundle();
            String name = getResources().getResourceEntryName(v.getId());
            String letnik = ((Button) v).getText().toString();

            if (Integer.parseInt(v.getTag().toString()) < 4)
                letnik = "[UNI] " + letnik;
            else
                letnik = "[VS] " + letnik;

            b.putString("letnik", name);
            b.putString("letnik2", letnik);
            intent.putExtras(b);
            startActivity(intent);
        }
    };

    void checkLogin(View v) {
        Context context = getApplicationContext();
        StringBuilder sb = new StringBuilder();
        String line;
        BufferedReader in = null;
        boolean login;

        try {
            in = new BufferedReader(new FileReader(new File(context.getFilesDir(), "login")));
            while ((line = in.readLine()) != null) sb.append(line + "\n");
            in.close();
            login = true;

        } catch (Exception e) {
            login = false;
        }

        if (login) {
            String[] user = sb.toString().split("\n");
            ((TextView) v.findViewById(R.id.currentUserName)).setText(user[2]);
            ((TextView) v.findViewById(R.id.currentUserEmail)).setText(user[1]);
        } else {
            Intent il = new Intent(main.this, Login.class);
            startActivity(il);
            main.this.finish();
        }
    }
}
